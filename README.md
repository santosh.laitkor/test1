# Passit Mobile

## Development

Ensure you have nativescript installed and a working Android SDK with emulator or device.

After cloning the repo:

1. `git submodule init`
2. `git submodule update`
3. `tns build android`
4. `tns run android`
