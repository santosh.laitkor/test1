import { Routes } from "@angular/router";

import { LoginContainer } from "./passit-frontend/account/login/login.container";
import { SecretListContainer } from "./passit-frontend/list/list.container";
import { GroupContainer } from "./passit-frontend/group/group.container";
import { SecretDetailComponent } from "./list"
import { LoggedInGuard } from "./passit-frontend/guards";

export const routes: Routes = [
    { path: "", redirectTo: "/list", pathMatch: "full" },
    {
        path: "login",
        component: LoginContainer,
        data: {
            title: "Login"
        }
    },
    {
        path: "list",
        component: SecretListContainer,
        canActivate: [LoggedInGuard],
    },
    {
        path: "list/:id",
        component: SecretDetailComponent,
        canActivate: [LoggedInGuard],
    },
    {
        path: "groups",
        component: GroupContainer,
        canActivate: [LoggedInGuard],
    },
];
