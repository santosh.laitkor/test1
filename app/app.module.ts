require( "nativescript-localstorage" );

import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { EffectsModule } from '@ngrx/effects';
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";

import { routes } from "./app.routing";
import { AppComponent } from "./app.component";

import { AccountModule } from "./account";
import { ListModule } from "./list";
import { GroupModule } from "./group/";
import { reducers } from "./app.reducers";
import { environment } from "./environments/environment";

import { Api } from "./passit-frontend/ngsdk/api";
import { NgPassitSDK } from "./passit-frontend/ngsdk/sdk";
import { LoggedInGuard } from "./passit-frontend/guards";
import { SecretService } from "./passit-frontend/secrets/secret.service";
import { GroupService } from "./passit-frontend/group/group.service";
import { metaReducers } from "./passit-frontend/app.reducers";
import { AppDataService } from "./passit-frontend/shared/app-data/app-data.service";
import { GetConfService } from "./passit-frontend/get-conf/";
import { SecretEffects } from "./passit-frontend/secrets/secret.effects";

import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

import { IsEmailDirective, MinLengthDirective } from "./directives";
import { MobileMenuModule } from "./mobile-menu";

@NgModule({
	bootstrap: [
		AppComponent
	],
	imports: [
		NativeScriptModule,
		AccountModule,
		ListModule,
		GroupModule,
		MobileMenuModule,
		EffectsModule.forRoot([SecretEffects]),
		StoreModule.forRoot(reducers, {metaReducers}),
		NativeScriptRouterModule.forRoot(routes),
		NativeScriptRouterModule,
		!environment.production ? StoreDevtoolsModule.instrument({maxAge: 25}) : [],
		NativeScriptHttpClientModule,
		NativeScriptFormsModule,
	],
	declarations: [
		AppComponent,
		IsEmailDirective,
		MinLengthDirective,
	],
	providers: [
		Api,
		AppDataService,
		LoggedInGuard,
		GetConfService,
		NgPassitSDK,
		SecretService,
		GroupService,
	],
	schemas: [NO_ERRORS_SCHEMA]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
