import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule} from "nativescript-angular/forms"
import { NativeScriptHttpModule } from "nativescript-angular/http";

import { UserService } from "../passit-frontend/account/user";
import { LoginContainer } from "../passit-frontend/account/login/login.container";
import { LoginComponent } from "./login";
import { LoginEffects } from "../passit-frontend/account/account.effects";
import { reducers } from "../passit-frontend/account/account.reducer";

export const COMPONENTS = [
  LoginContainer,
  LoginComponent,
];

export const SERVICES = [
  UserService,
];

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptHttpModule,
    StoreModule.forFeature('account', reducers),
    EffectsModule.forFeature([LoginEffects]),
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [SERVICES],
	schemas: [NO_ERRORS_SCHEMA]
})
export class AccountModule { }
