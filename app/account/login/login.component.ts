import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { ILoginForm } from "../../passit-frontend/account/login/interfaces";
import { NgModel } from '@angular/forms';

@Component({
  selector: "login-component",
  moduleId: module.id,
  templateUrl: "./login.component.html",
})
export class LoginComponent {
  @Input() errorMessage: string;
  @Input() hasLoginStarted: boolean;
  @Input() hasLoginFinished: boolean;
  @Output() login = new EventEmitter<ILoginForm>();
  @Output() goToRegister = new EventEmitter();
  // https://github.com/NativeScript/nativescript-angular/issues/905
  emailDirty = false;
  passwordDirty = false;

  form: ILoginForm;
  @ViewChild("email") email: ElementRef;
  @ViewChild("emailModel") emailModel: NgModel;
  @ViewChild("password") password: ElementRef;
  @ViewChild("passwordModel") passwordModel: NgModel;

  constructor() {
    this.form = {
      email: "",
      password: "",
      url: "app.passit.io", 
    };
  }

  onSubmit() {
    if (this.emailModel.valid && this.passwordModel.valid) {
      this.form.rememberMe = true;
      this.login.emit(this.form);
    }
  }

  emailOnBlur() {
    this.emailDirty = true;
  }

  passwordOnBlur() {
    this.passwordDirty = true;
  }

  focusPassword() {
    this.password.nativeElement.focus();
  }
}
