import { Component } from "@angular/core";

import { AppDataService } from "./passit-frontend/shared/app-data/app-data.service";

@Component({
  selector: "ns-app",
  templateUrl: "app.component.html",
})
export class AppComponent {
  constructor(private appDataService: AppDataService) {
    this.appDataService.rehydrate();
  }
}
