import { Component, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "ns-action-bar-component",
  moduleId: module.id,
  templateUrl: "./action-bar.component.html",
})
export class ActionBarComponent {
  @Output() menuTap = new EventEmitter();

  constructor() {}
}
