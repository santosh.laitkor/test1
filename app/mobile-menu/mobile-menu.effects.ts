import { Injectable } from "@angular/core";
import { Actions, Effect } from "@ngrx/effects";

import { ToggleMenu, MobileMenuActionTypes } from "./mobile-menu.actions";
import { SideDrawer } from "./sidedrawer";

@Injectable()
export class MobileMenuEffects {

  /**
   * :( We can't control the side menu declaratively.
   * We can't determine the current state it's in (open or closed)
   * All we can do is toggle it - which makes it a side effect.
   */
  @Effect({ dispatch: false })
  toggleMenu$ = this.actions$
    .ofType<ToggleMenu>(MobileMenuActionTypes.TOGGLE_MENU)
    .do(() => {
      this.sidedrawer.toggle();
    });

  constructor(
    private actions$: Actions,
    private sidedrawer: SideDrawer, 
  ) {}
}
