import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { TnsSideDrawer } from 'nativescript-foss-sidedrawer';
import { NgZone } from "@angular/core";

import * as reducers from "./mobile-menu.reducers";
import { LogoutAction } from "../passit-frontend/account/account.actions";

/**
 * Declarative Wrapper around TnsSideDrawer
 * A service works well because it should be a singleton and there is no template.
 * Every action to the drawer can be done via state changes
 */
@Injectable()
export class SideDrawer {
  private TnsSideDrawer = TnsSideDrawer;
  // private isOpen = this.store.select(reducers.getMobileMenuShowMenu);

  constructor(
    private store: Store<reducers.IMobileMenuState>,
    private router: Router,
    private zone: NgZone,
  ) {}

  /**
   * Run this exactly once.
   * @param context - The 'this' from a NS Component.
   */
  build(context: any) {
    this.TnsSideDrawer.build({
      templates: [
        {title: "Passwords"},
        {title: "Groups"},
        {title: "Log Out"},
     ],
      title: 'Passit',
      listener: (index) => {
        switch (index) {
          case 0:
            this.zone.run(() =>
              this.router.navigate(["/list"]));
            return
          case 1:
            this.zone.run(() =>
              this.router.navigate(["/groups"]));
            return
          case 2:
            this.store.dispatch(new LogoutAction());
            return
        }
      },
      // context,
    })
    // this.isOpen.subscribe((isOpen) => {
    //   if (isOpen) {
    //     this.open();
    //   } else {
    //     this.close();
    //   }
    // });
  }

  toggle() {
    this.TnsSideDrawer.toggle(true);
  }

  // private open() {
  //   this.TnsSideDrawer.open();
  // }

  // private close() {
  //   this.TnsSideDrawer.close();
  // }
}
