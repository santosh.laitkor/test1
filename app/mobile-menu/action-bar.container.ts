import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";

import * as fromRoot from "../app.reducers";
import { ToggleMenu } from "./mobile-menu.actions";
import { SideDrawer } from "./sidedrawer";

@Component({
  selector: "ns-action-bar-container",
  moduleId: module.id,
  template: `
    <ns-action-bar-component
      [showSearch]="showSearch$ | async"
      (menuTap)="menuTap()"
      (searchTap)="searchTap()"
      (search)="search($event)"
    ></ns-action-bar-component>
  `,
})
export class ActionBarContainer implements OnInit {
  constructor(
    private store: Store<fromRoot.IState>,
    private sidedrawer: SideDrawer, 
  ) { }

  ngOnInit() {
    // Pass the component's context to the service
    setTimeout(() => this.sidedrawer.build(this), 100);
  }

  menuTap() {
      this.store.dispatch(new ToggleMenu());
  }
}

