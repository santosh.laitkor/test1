import { ActionReducerMap } from "@ngrx/store";

import * as confState from "./passit-frontend/get-conf/conf.reducer";
import * as contactsState from "./passit-frontend/group/contacts/contacts.reducer";
import * as groupState from "./passit-frontend/group/group.reducer";
import * as secretState from "./passit-frontend/secrets/secrets.reducer";

export interface IState {
    contacts: contactsState.IContactsState;
    secrets: secretState.ISecretState;
    group: groupState.IGroupState;
    conf: confState.IConfState;
}

export const reducers: ActionReducerMap<IState> = {
    contacts: contactsState.contactsReducer,
    group: groupState.groupReducer,
    secrets: secretState.secretReducer,
    conf: confState.reducer,
};
