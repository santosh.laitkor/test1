import { Component, Input } from "@angular/core";

import * as Clipboard from "nativescript-clipboard";

import * as api from "passit-sdk-js/js/api.interfaces";

import { Secret } from "../passit-frontend/secrets";
import { SecretService } from "../passit-frontend/secrets/secret.service";

@Component({
  selector: "secret-form-component",
  moduleId: module.id,
  templateUrl: "./form.component.html",
})
export class SecretFormComponent {
  secret: Secret;
  passwordIsMasked = true;
  plaintextSecrets = {};

  constructor(private secretService: SecretService) {}

  @Input()
  set secretManaged(dbSecret: api.ISecret) {
    this.passwordIsMasked = true;
    this.plaintextSecrets = {};
    this.secret = new Secret(dbSecret);
    if (this.secret.id) {
      this.secretService.showOfflineSecret(this.secret.dbSecret).then((secrets) => {
        this.plaintextSecrets = this.secretService.setPlaintextSecrets(secrets);
      });
    }
  }

  toggleMaskedSecret() {
    this.passwordIsMasked = !this.passwordIsMasked;
  }

  copy(value: string) {
    return Clipboard.setText(value);
  }
}
