import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import { getSecretManaged } from "./selectors";
// import * as fromRoot from "../app.reducers";
import * as listReducer from "../passit-frontend/list/list.reducer";

/** This is only used in phone layouts
 * It doesn't follow the general smart/dumb comoponent split because
 * there is no web version of this - so we just combine it.
 */
@Component({
  selector: "secret-detail-component",
  moduleId: module.id,
  templateUrl: "./detail.component.html",
})
export class SecretDetailComponent {
  secretManaged$ = this.store.select(getSecretManaged);

  constructor(
    public store: Store<listReducer.IListState>
  ) {}
}
