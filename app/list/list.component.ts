import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { DeviceType } from "ui/enums";
import { device } from "platform";

import * as api from "passit-sdk-js/js/api.interfaces";
import { Secret } from "../passit-frontend/secrets";
import { Router } from "@angular/router";
import { SideDrawer } from "../mobile-menu/sidedrawer";


@Component({
  selector: "secret-list-component",
  moduleId: module.id,
  templateUrl: "./list.component.html",
})
export class SecretListComponent implements OnInit {
  @Input() secrets: api.ISecret[];
  @Input() secretManagedObject: api.ISecret[];
  @Output() secretWasSelected = new EventEmitter<Secret>();
  isTablet: boolean = device.deviceType === DeviceType.Tablet;

  constructor(private router: Router, private sidedrawer: SideDrawer) {}

  ngOnInit() {
    setTimeout(() => this.sidedrawer.build(this), 100);
    setTimeout(() => this.sidedrawer.build(this), 1000);
  }

  onSecretWasSelected(secret: Secret) {
    this.secretWasSelected.emit(secret);
    if (!this.isTablet) {
      // On phone - send to a new page
      this.router.navigate(['/list', secret.id])
    }
  }
}