import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { StoreModule } from '@ngrx/store';

import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule} from "nativescript-angular/forms"

import { SecretListContainer } from "../passit-frontend/list/list.container";
import { SecretListComponent } from "./list.component";
import { SecretFormComponent } from "./form.component";
import { SecretDetailComponent } from "./detail.component";
import { listReducer } from "../passit-frontend/list/list.reducer";
import { MobileMenuModule } from "../mobile-menu"
import { ListActionBarContainer, ListActionBarComponent } from "./actionbar";

export const COMPONENTS = [
  SecretListComponent,
  SecretListContainer,
  SecretFormComponent,
  SecretDetailComponent,
  ListActionBarContainer,
  ListActionBarComponent,
];

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    MobileMenuModule,
    StoreModule.forFeature('list', listReducer),
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [],
	schemas: [NO_ERRORS_SCHEMA]
})
export class ListModule { }
